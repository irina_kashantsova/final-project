import {createButton} from './common.js';

export const navbar = (title, logoSrc, buttonOne, buttonTwo) => {
    // creating elements
    const navbar = document.createElement('navbar');
    const imageLogo = document.createElement('img');
    const divImage = document.createElement('div');
    const divList = document.createElement('div');
    const divLogoTitle = document.createElement('div');

    navbar.classList.add('account-navbar');

    imageLogo.setAttribute('src', logoSrc);
    imageLogo.classList.add('account-navbar__image', 'account-navbar__image-div');

    divList.classList.add('account-navbar__buttons');

    divLogoTitle.innerHTML = title;
    divLogoTitle.classList.add('account-navbar__logo-title');

    // placing elements
    divImage.append(imageLogo);
    navbar.append(divList);
    navbar.prepend(divLogoTitle);
    navbar.prepend(divImage);
    divList.append(buttonOne);
    divList.append(buttonTwo);

    return navbar;
};

export const createSignUpButton = () => {
    const signUpButton = createButton('Sign Up', 'button', 'account-navbar__sign-up-button');

    return signUpButton;
};

export const createLoginInButton = () => {
    const loginInButton = createButton('Login In', 'button', 'account-navbar__login-in-button');

    return loginInButton;
};
