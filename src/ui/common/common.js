//creating function for create buttons
export const createButton = (text, type, classNameModify, className = 'sign-up-form__buttons-item') => {
    const button = document.createElement('button');
    button.setAttribute('type', type);
    button.classList.add(className);
    button.classList.add(classNameModify);
    button.textContent = text;

    return button;
};

//creating function for create titles
export const createTitle = (text, className, tagName) => {
    const title = document.createElement(tagName);
    title.textContent = text;
    title.classList.add(className);

    return title;
};

export const divWrapper = document.querySelector('#root');
