import {createButton, createTitle, divWrapper} from '../../common/common.js';
import {navbar, createSignUpButton, createLoginInButton} from '../../common/navbar.js';
import {signUpFunction, logIn} from '../engine/engine.js';

const createForm = (divInputs, firstTitle, secondTitle, flag) => {
    const clickOnSignUpButton = () => {
        divForm.replaceWith(createSignUpForm());
        document.querySelector('.account-navbar__sign-up-button').hidden = true;
        document.querySelector('.account-navbar__login-in-button').hidden = false;

        setTimeout(function () {
            document.querySelector('.login-in-form').classList.add('appear');
        }, 500);
    };

    const clickOnLoginButton = () => {
        divForm.replaceWith(createLoginInForm());
        document.querySelector('.account-navbar__login-in-button').hidden = true;
        document.querySelector('.account-navbar__sign-up-button').hidden = false;

        setTimeout(function () {
            document.querySelector('.login-in-form').classList.add('appear');
        }, 500);
    };

    const clearForm = () => {
        const errors = document.querySelectorAll('.sign-up-form__error');
        for (let i = 0; i < errors.length; i++) {
            errors[i].remove();
        }
    };
    //creating elements
    const divForm = document.createElement('div');
    const form = document.createElement('form');
    const divTitles = document.createElement('div');
    const divButtons = document.createElement('div');
    const signUpLoginInButton = createButton(secondTitle, 'submit');
    const clearButton = createButton('Clear', 'reset', 'button-clear');
    const navSignUpButton = createSignUpButton();
    const navLoginInButton = createLoginInButton();

    navSignUpButton.addEventListener('click', clickOnSignUpButton);
    navLoginInButton.addEventListener('click', clickOnLoginButton);
    clearButton.addEventListener('click', clearForm);

    if (flag === 'signUp') {
        signUpLoginInButton.addEventListener('click', signUpFunction);
    } else if (flag === 'loginIn') {
        signUpLoginInButton.addEventListener('click', logIn);
    }

    //adding class names to elements
    divForm.classList.add('div-login-in-form');
    form.classList.add('login-in-form');
    divTitles.classList.add('login-in-form__titles');
    divButtons.classList.add('login-in-form__buttons');

    //placing elements
    divWrapper.append(divForm);
    divTitles.append(createTitle(firstTitle, 'first-title', 'h2'));
    divTitles.append(createTitle(secondTitle, 'second-title', 'h3'));
    form.append(divTitles);
    form.append(divInputs);
    divButtons.append(signUpLoginInButton);
    divButtons.append(clearButton);
    form.append(divButtons);
    divForm.append(
        navbar(
            'Welcome to the Universe',
            'https://edifian.digital/wp-content/uploads/2020/05/edifian-orb.gif',
            navSignUpButton,
            navLoginInButton
        )
    );
    divForm.append(form);

    return divForm;
};

export const createSignUpForm = () => {
    return createForm(signUpInputs(), 'Are you new to the Universe?', 'Sign up', 'signUp');
};

export const createLoginInForm = () => {
    return createForm(loginInputs(), 'Already has an account?', 'Login in', 'loginIn');
};

//creating function for create inputs
export const createInput = (placeholder, type, className) => {
    const input = document.createElement('input');

    input.classList.add('sign-up-form__inputs-item');
    input.classList.add(className);
    input.setAttribute('placeholder', placeholder);
    type && input.setAttribute('type', type);

    return input;
};

export const loginInputs = () => {
    const divInputs = document.createElement('div');

    divInputs.classList.add('login-in-form__inputs');
    divInputs.append(createInput('Enter login', 'text', 'login-in__login-input'));
    divInputs.append(createInput('Enter password', 'password', 'login-in__password-input'));

    return divInputs;
};

export const signUpInputs = () => {
    const divInputs = document.createElement('div');

    divInputs.classList.add('login-in-form__inputs');
    divInputs.append(createInput('Enter login', 'text', 'sign-in__login-input'));
    divInputs.append(createInput('Enter password', 'password', 'sign-in__password-input'));
    divInputs.append(createInput('Repeat password', 'password', 'sign-in__repeat-password-input'));

    return divInputs;
};
