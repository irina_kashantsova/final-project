import {createTitle} from '../../common/common.js';
import {createLoginInForm} from '../form/form.js';
import {loginInputs} from '../form/form.js';
import {drawMainPage} from '../../content/mainPage/mainPage.js';
import {mainPageTitles} from '../../../resources/data/data.js';

export const logIn = (e) => {
    localStorage.setItem('logged', null);

    e.preventDefault();

    const loginFormLoginInputValue = document.querySelector('.login-in__login-input').value;
    const loginFormPasswordInputValue = document.querySelector('.login-in__password-input').value;

    const removeValidation = () => {
        const errors = document.querySelectorAll('.sign-up-form__error');
        for (let i = 0; i < errors.length; i++) {
            errors[i].remove();
        }
    };

    removeValidation();

    if (loginFormLoginInputValue === '') {
        document.querySelector('.login-in-form').append(createTitle('Enter login', 'sign-up-form__error', 'h4'));
    }

    if (loginFormPasswordInputValue === '') {
        document.querySelector('.login-in-form').append(createTitle('Enter password', 'sign-up-form__error', 'h4'));
    }

    if (loginFormLoginInputValue != '' && loginFormPasswordInputValue != '') {
        const localStorageItem = JSON.parse(localStorage.getItem(loginFormLoginInputValue));

        if (localStorageItem === null) {
            document
                .querySelector('.login-in-form')
                .append(createTitle("Account doesn't exist", 'sign-up-form__error', 'h4'));
        } else {
            if (localStorageItem.password === loginFormPasswordInputValue) {
                document.querySelector('.div-login-in-form').remove();
                localStorage.setItem('logged', loginFormLoginInputValue);
                drawMainPage(mainPageTitles);
            } else {
                document
                    .querySelector('.login-in-form')
                    .append(createTitle('Wrong password', 'sign-up-form__error', 'h4'));
            }
        }
    }
};

export const signUpFunction = (e) => {
    e.preventDefault();

    const loginInputValue = document.querySelector('.sign-in__login-input');
    const passwordInputValue = document.querySelector('.sign-in__password-input');
    const passwordRepeatInputValue = document.querySelector('.sign-in__repeat-password-input');

    const removeValidation = () => {
        const errors = document.querySelectorAll('.sign-up-form__error');
        for (let i = 0; i < errors.length; i++) {
            errors[i].remove();
        }
    };

    removeValidation();

    if (loginInputValue.value === '') {
        document.querySelector('.login-in-form').append(createTitle('Enter login', 'sign-up-form__error', 'h4'));
    }

    if (passwordInputValue.value === '' || passwordRepeatInputValue.value === '') {
        document.querySelector('.login-in-form').append(createTitle('Enter password', 'sign-up-form__error', 'h4'));
    }

    if (passwordInputValue.value != passwordRepeatInputValue.value) {
        document
            .querySelector('.login-in-form')
            .append(createTitle("Passwords don't match", 'sign-up-form__error', 'h4'));
    }

    if (localStorage.getItem(loginInputValue.value)) {
        document
            .querySelector('.login-in-form')
            .append(createTitle('Account already exists', 'sign-up-form__error', 'h4'));
    }

    if (
        !localStorage.getItem(loginInputValue.value) &&
        loginInputValue.value != '' &&
        passwordInputValue.value === passwordRepeatInputValue.value &&
        passwordInputValue.value != '' &&
        passwordRepeatInputValue.value != ''
    ) {
        setTimeout(function () {
            document.querySelector('.login-in-form').classList.add('appear');
        }, 500);
        localStorage.setItem(loginInputValue.value, JSON.stringify({password: passwordInputValue.value}));
        document.querySelector('.div-login-in-form').replaceWith(createLoginInForm(loginInputs()));
        document
            .querySelector('form')
            .append(createTitle('Account was successfully created', 'sign-up-form__success', 'h4'));
    }
};
