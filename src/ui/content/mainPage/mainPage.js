import {createTitle, createButton, divWrapper} from '../../common/common.js';
import {createSlider} from '../solarSystem/slider.js';
import {navbar} from '../../common/navbar.js';
import {arrayImages} from '../../../resources/data/data.js';
import {drawGreetingPage} from '../greetingPage/greetingPage.js';

export const drawMainPage = (mainPageTitles) => {
    const divMainPage = document.createElement('div');
    const divTitlesWrapper = document.createElement('div');
    const divMainPageTitle = document.createElement('div');
    const divMainPageSecondTitle = document.createElement('div');
    const divMainPageThirdTitle = document.createElement('div');
    const buttonExplore = createButton('Explore', 'button', 'main-page__navbar-button-explore');
    const buttonLogOut = createButton('Log out', 'button', 'main-page__navbar-button-log-out');

    const createExplorePage = () => {
        divMainPage.remove();
        const divExplorePage = document.createElement('div');

        divExplorePage.append(createSlider(arrayImages));
        divWrapper.append(divExplorePage);
        return divExplorePage;
    };

    divMainPage.classList.add('main-page__div-wrapper');
    divTitlesWrapper.classList.add('main-page__div-titles-wrapper');
    divMainPageTitle.classList.add('main-page__div-title');
    divMainPageSecondTitle.classList.add('main-page__div-second-title');
    divMainPageThirdTitle.classList.add('main-page__div-third-title');

    divMainPage.append(
        navbar(
            'Welcome to the Universe',
            'https://edifian.digital/wp-content/uploads/2020/05/edifian-orb.gif',
            buttonExplore,
            buttonLogOut
        )
    );

    const logOut = () => {
        localStorage.setItem('logged', 'null');
        divMainPage.remove();
        divWrapper.append(
            drawGreetingPage(
                'Welcome to the Universe',
                'https://edifian.digital/wp-content/uploads/2020/05/edifian-orb.gif'
            )
        );
    };
    buttonExplore.addEventListener('click', createExplorePage);
    buttonLogOut.addEventListener('click', logOut);

    divMainPageTitle.append(createTitle(mainPageTitles.firstTitle, 'main-page__title', 'h2'));
    divMainPageSecondTitle.append(createTitle(mainPageTitles.secondTitle, 'main-page__second-title', 'h3'));
    divMainPageThirdTitle.append(createTitle(mainPageTitles.thirdTitle, 'main-third-title', 'h4'));
    divMainPage.append(divTitlesWrapper);
    divTitlesWrapper.append(divMainPageTitle);
    divTitlesWrapper.append(divMainPageSecondTitle);
    divTitlesWrapper.append(divMainPageThirdTitle);
    divWrapper.append(divMainPage);

    return divMainPage;
};
