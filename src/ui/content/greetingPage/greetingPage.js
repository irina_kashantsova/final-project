import {createSignUpForm, createLoginInForm} from '../../account/form/form.js';
import {createButton, createTitle, divWrapper} from '../../common/common.js';
import {navbar, createSignUpButton, createLoginInButton} from '../../common/navbar.js';
import {quotes} from '../../../resources/data/data.js';

export const drawGreetingPage = () => {
    const divGreetingPage = document.createElement('div');
    const greetingPageContent = document.createElement('div');
    const divGreetingLogo = document.createElement('div');
    const divGreetingTitle = document.createElement('div');
    const divTextWrapper = document.createElement('div');
    const greetingLogo = document.createElement('img');
    const divParagraphQuote = document.createElement('div');
    const divAuthorQuote = document.createElement('div');
    const divQuoteContainer = document.createElement('div');
    const paragraphQuote = document.createElement('p');
    const paragraphAuthor = document.createElement('p');
    const navSignUpButton = createSignUpButton();
    const navLoginInButton = createLoginInButton();
    //functions
    const clickOnSignUpButton = () => {
        divGreetingPage.replaceWith(createSignUpForm());
        document.querySelector('.account-navbar__sign-up-button').hidden = true;
        document.querySelector('.account-navbar__login-in-button').hidden = false;
        setTimeout(function() {
            document.querySelector('.login-in-form').classList.add('appear');
        }, 500)
    };

    const clickOnLoginButton = () => {
        divGreetingPage.replaceWith(createLoginInForm());
        document.querySelector('.account-navbar__login-in-button').hidden = true;
        document.querySelector('.account-navbar__sign-up-button').hidden = false;
        setTimeout(function() {
            document.querySelector('.login-in-form').classList.add('appear');
        }, 500)

    };

    const generatingQuote = () => {
        const quoteIndex = parseInt(Math.random() * quotes.length);

        paragraphQuote.textContent = quotes[quoteIndex].quote;
        paragraphAuthor.textContent = quotes[quoteIndex].author;
    };

    generatingQuote();

    navSignUpButton.addEventListener('click', clickOnSignUpButton);
    navLoginInButton.addEventListener('click', clickOnLoginButton);

    greetingPageContent.classList.add('greeting-page__content');
    divGreetingLogo.classList.add('greeting-page__div-greeting-logo');
    divGreetingTitle.classList.add('greeting-page__div-greeting-title');
    divTextWrapper.classList.add('greeting-page__div-text-wrapper');
    divParagraphQuote.classList.add('greeting-page__div-paragraph-quote');
    divAuthorQuote.classList.add('greeting-page__div-paragraph-author');
    divQuoteContainer.classList.add('greeting-page__div-quote-container');
    greetingLogo.classList.add('greeting-page__big-logo');
    greetingLogo.setAttribute('src', 'https://edifian.digital/wp-content/uploads/2020/05/edifian-orb.gif');
    divGreetingPage.classList.add('greeting-page__div-wrapper');

    divGreetingPage.append(
        navbar(
            'Welcome to the Universe',
            'https://edifian.digital/wp-content/uploads/2020/05/edifian-orb.gif',
            navSignUpButton,
            navLoginInButton
        )
    );

    // greetingPageContent.append(divGreetingLogo);
    divTextWrapper.append(divGreetingTitle);
    divTextWrapper.append(divQuoteContainer);
    divGreetingTitle.append(createTitle("Take a look. It's all around you.", 'greeting-page__title', 'h2'));
    divQuoteContainer.append(divParagraphQuote);
    divQuoteContainer.append(divAuthorQuote);
    divParagraphQuote.append(paragraphQuote);
    divAuthorQuote.append(paragraphAuthor);
    divGreetingLogo.append(greetingLogo);
    divGreetingPage.append(greetingPageContent);
    greetingPageContent.append(divTextWrapper);
    greetingPageContent.append(divGreetingLogo);
    divWrapper.append(divGreetingPage);

    return divGreetingPage;
};
