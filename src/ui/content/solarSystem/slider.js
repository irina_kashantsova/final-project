import {navbar} from '../../common/navbar.js';
import {createButton, divWrapper} from '../../common/common.js';
import {drawGreetingPage} from '../greetingPage/greetingPage.js';

export const createSlider = (arrayImages) => {
    const mainDivSlider = document.createElement('div');
    const sliderContent = document.createElement('div');
    const divImage = document.createElement('div');
    const planetInfo = document.createElement('div');
    const titleName = document.createElement('h2');
    const planetImage = document.createElement('img');
    const prevButton = document.createElement('i');
    const nextButton = document.createElement('i');
    const buttonLogOut = createButton('Log out', 'button', 'main-page__navbar-button-log-out');

    let counter = 0;

    sliderContent.classList.add('solar-system-page__slider-content');
    mainDivSlider.classList.add('solar-system-page__div-content');
    divImage.classList.add('solar-system-page__div-image');
    planetImage.classList.add('solar-system-page__image');
    prevButton.classList.add('fas', 'fa-angle-left');
    nextButton.classList.add('fas', 'fa-angle-right');
    planetInfo.classList.add('solar-system-page__info');
    titleName.classList.add('solar-system-page__title');

    planetImage.setAttribute('src', arrayImages[counter].image);
    planetInfo.textContent = arrayImages[counter].info;
    titleName.textContent = arrayImages[counter].title;

    const getNextImg = () => {
        counter++;
        planetImage.setAttribute('src', arrayImages[counter].image);
        planetInfo.textContent = arrayImages[counter].info;
        titleName.textContent = arrayImages[counter].title;

        if (counter === arrayImages.length - 1) {
            nextButton.style.display = 'none';
        }
        if (counter > 0) {
            prevButton.style.display = 'inline';
        }
    };

    const getPrevImg = () => {
        counter--;
        planetImage.setAttribute('src', arrayImages[counter].image);
        planetInfo.textContent = arrayImages[counter].info;
        titleName.textContent = arrayImages[counter].title;

        if (counter === 0) {
            prevButton.style.display = 'none';
        }
        if (counter < arrayImages.length - 1) {
            nextButton.style.display = 'inline';
        }
    };

    const logOut = () => {
        localStorage.setItem('logged', 'null');
        mainDivSlider.remove();
        divWrapper.append(
            drawGreetingPage(
                'Welcome to the Universe',
                'https://edifian.digital/wp-content/uploads/2020/05/edifian-orb.gif'
            )
        );
    };

    nextButton.addEventListener('click', getNextImg);
    prevButton.addEventListener('click', getPrevImg);
    buttonLogOut.addEventListener('click', logOut);
    mainDivSlider.append(
        navbar(
            'Welcome to the Universe',
            'https://edifian.digital/wp-content/uploads/2020/05/edifian-orb.gif',
            '',
            buttonLogOut
        )
    );

    mainDivSlider.append(sliderContent);
    sliderContent.append(titleName);
    sliderContent.append(divImage);
    sliderContent.append(planetInfo);
    divImage.append(prevButton);
    divImage.append(planetImage);
    divImage.append(nextButton);
    divWrapper.append(mainDivSlider);

    return mainDivSlider;
};
