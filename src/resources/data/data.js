export const quotes = [
    {
        quote: '"Two things are infinite: the universe and human stupidity; and I\'m not sure about the universe."',
        author: 'Albert Einstein'
    },
    {
        quote:
            '"Two possibilities exist: either we are alone in the Universe or we are not. Both are equally terrifying."',
        author: 'Arthur C. Clarke'
    },
    {
        quote: '"I\'m sure the universe is full of intelligent life. It\'s just been too intelligent to come here."',
        author: 'Arthur C. Clarke'
    },
    {
        quote:
            '\"The only thing that scares me more than space aliens is the idea that there aren\'t any space aliens. We can\'t be the best that creation has to offer. I pray we\'re not all there is. If so, we\'re in big trouble.\"',
        author: 'Ellen DeGeneres'
    },
    {
        quote:
            '\"Every one of us is, in the cosmic perspective, precious. If a human disagrees with you, let him live. In a hundred billion galaxies, you will not find another.\"',
        author: 'Carl Sagan'
    },
    {
        quote:
            '\"The universe is a pretty big place. If it\'s just us, seems like an awful waste of space.\"',
        author: 'Carl Sagan'
    },
];

export const mainPageTitles = {
    firstTitle: 'When you look up at the bright stars in the nighttime sky, do you WONDER what\'s out there?',
    secondTitle:
        'Who hasn\'t felt a sense of awe while looking deep into the sky, lit with countless stars on a clear night? Who hasn\'t asked themselves if ours is the only planet that supports life?',
    thirdTitle:
        'Learning about the solar system can allow everyone to gain a greater appreciation for the frailness of Earth.'
};

export const arrayImages = [
    {
        image: './src/resources/img/slider/andromeda.jpg',
        title: 'Andromeda Galaxy',
        info: 'Andromeda Galaxy, also called Andromeda Nebula, (catalog numbers NGC 224 and M31), great spiral galaxy in the constellation Andromeda, the nearest large galaxy. The Andromeda Galaxy is one of the few visible to the unaided eye, appearing as a milky blur. It is located about 2,480,000 light-years from Earth; its diameter is approximately 200,000 light-years; and it shares various characteristics with the Milky Way system.'
    },
    {
        image: './src/resources/img/slider/antennae.jpg',
        title: 'Antennae Galaxies',
        info: 'The two spiral galaxies started to interact a few hundred million years ago, making the Antennae galaxies one of the nearest and youngest examples of a pair of colliding galaxies. Nearly half of the faint objects in the Antennae image are young clusters containing tens of thousands of stars. The orange blobs to the left and right of image center are the two cores of the original galaxies and consist mainly of old stars criss-crossed by filaments of dust.'
    },
    {
        image: './src/resources/img/slider/blackEye.png',
        title: 'Black Eye Galaxy',
        info: 'The Black Eye Galaxy (also called Evil Eye Galaxy and designated Messier 64, M64, or NGC 4826) is a relatively isolated spiral galaxy located 17 million light years away in the northern constellation of Coma Berenices. It was discovered by Edward Pigott in March 1779, and independently by Johann Elert Bode in April of the same year, as well as by Charles Messier in 1780. A dark band of absorbing dust in front of the galaxy\'s bright nucleus gave rise to its nicknames of the "Black Eye" or "Evil Eye" galaxy.'
    },
    {
        image: './src/resources/img/slider/messierjpg.jpg',
        title: 'Messier 81',
        info: 'Discovered by the German astronomer Johann Elert Bode in 1774, M81 is one of the brightest galaxies in the night sky. It is located 11.6 million light-years from Earth in the constellation Ursa Major and has an apparent magnitude of 6.9. Through a pair of binoculars, the galaxy appears as a faint patch of light in the same field of view as M82. A small telescope will resolve M81’s core. The galaxy is best observed during April.'
    },
    {
        image: './src/resources/img/slider/milkyWay.jpg',
        title: 'Milky Way',
        info: 'Milky Way Galaxy, large spiral system consisting of several hundred billion stars, one of which is the Sun. It takes its name from the Milky Way, the irregular luminous band of stars and gas clouds that stretches across the sky as seen from Earth. Although Earth lies well within the Milky Way Galaxy (sometimes simply called the Galaxy), astronomers do not have as complete an understanding of its nature as they do of some external star systems.'
    },
    {
        image: './src/resources/img/slider/bubble_nebula.jpg',
        title: 'Bubble Nebula',
        info: 'The Bubble Nebula is an emission nebula located in the northern constellation Cassiopeia. The nebula lies at a distance of 7,100 to 11,000 light years from Earth. It was nicknamed the Bubble Nebula because of its shape, which was created by a strong stellar wind from a young, massive, hot Wolf-Rayet star that shed its material to form the nebula.'
    },
    {
        image: './src/resources/img/slider/carina_nebula.jpg',
        title: 'Carina Nebula',
        info: 'The Carina Nebula was discovered by the French astronomer Abbe Nicolas-Louis de Lacaille in 1752. In addition to discovering and naming this nebula, the Abbe also discovered and named 13 other constellations in the 88 catalogued to date. The nebula has several nicknames, including the Grand Nebula, the Great Nebula in Carina and Eta Carinae Nebula. The Carina Nebula formation includes the Milky Way’s brightest star, WR 25 in the Trumpler 16 star cluster.'
    },
    {
        image: './src/resources/img/slider/eagle_nebula.jpg',
        title: 'The Eagle Nebula',
        info: 'The Eagle Nebula, also known as Messier 16 or M16, is one of the most amazing sights that can be seen in a large telescope. It’s the location of several famous structures including the stunning Pillars of Creation, an active star-forming region of gas and dust, depicted in the image above.'
    },
    {
        image: './src/resources/img/slider/orion_nebula.jpg',
        title: 'Orion Nebula',
        info: 'Believed to be the cosmic fire of creation by the Maya of Mesoamerica, M42 blazes brightly in the constellation Orion. Popularly called the Orion Nebula, this stellar nursery has been known to many different cultures throughout human history. The nebula is only 1,500 light-years away, making it the closest large star-forming region to Earth.'
    },
    {
        image: './src/resources/img/slider/homunculus_nebula.jpg',
        title: 'Homunculus Nebula',
        info: 'The Homunculus Nebula is a bipolar emission and reflection nebula surrounding the massive star system Eta Carinae, about 7,500 light-years (2,300 parsecs) from Earth. The nebula is embedded within the much larger Carina Nebula, a large star-forming H II region.'
    },
    {
        image: './src/resources/img/slider/cats-eye-nebula.jpg',
        title: 'Cat’s Eye Nebula',
        info: 'The Cat\'s Eye Nebula is a very bright nebula in the northern constellation Draco. The Cat\'s Eye Nebula has a complicated and beautiful structure made from multiple expanding spheres of gas. '
    },
    {
        image: './src/resources/img/slider/sunImage.jpg',
        title: 'The Sun',
        info: 'The Sun is the star at the center of the Solar System. It is a nearly perfect sphere of hot plasma, heated to incandescence by nuclear fusion reactions in its core, radiating the energy mainly as light and infrared radiation. It is by far the most important source of energy for life on Earth. Its diameter is about 1.39 million kilometers (864,000 miles), or 109 times that of Earth, and its mass is about 330,000 times that of Earth. It accounts for about 99.86% of the total mass of the Solar System.'
    },
    {
        image: './src/resources/img/slider/mercuryImage.jpg',
        title: 'Mercury',
        info: 'Mercury is the smallest planet in our solar system. It’s just a little bigger than Earth’s moon. It is the closest planet to the sun, but it’s actually not the hottest. Venus is hotter.'
    },
    {
        image: './src/resources/img/slider/venusImage.jpg',
        title: 'Venus',
        info: 'Even though Venus isn\'t the closest planet to the sun, it is still the hottest. It has a thick atmosphere full of the greenhouse gas carbon dioxide and clouds made of sulfuric acid. The gas traps heat and keeps Venus toasty warm. In fact, it\'s so hot on Venus, metals like lead would be puddles of melted liquid.'
    },
    {
        image: './src/resources/img/slider/earthImage.jpg',
        title: 'Earth',
        info: 'Our home planet Earth is a rocky, terrestrial planet. It has a solid and active surface with mountains, valleys, canyons, plains and so much more. Earth is special because it is an ocean planet. Water covers 70% of Earth\'s surface.'
    },
    {
        image: './src/resources/img/slider/marsImage.jpg',
        title: 'Mars',
        info: 'Mars is a cold desert world. It is half the size of Earth. Mars is sometimes called the Red Planet. It\'s red because of rusty iron in the ground.'
    },
    {
        image: './src/resources/img/slider/jupiterImage.jpg',
        title: 'Jupiter',
        info: 'Jupiter is the biggest planet in our solar system. It\'s similar to a star, but it never got big enough to start burning. It is covered in swirling cloud stripes. It has big storms like the Great Red Spot, which has been going for hundreds of years. Jupiter is a gas giant and doesn\'t have a solid surface, but it may have a solid inner core about the size of Earth. Jupiter also has rings, but they\'re too faint to see very well.'
    },
    {
        image: './src/resources/img/slider/saturn.jpg',
        title: 'Saturn',
        info: 'Saturn isn’t the only planet to have rings, but it definitely has the most beautiful ones. The rings we see are made of groups of tiny ringlets that surround Saturn. They’re made of chunks of ice and rock. Like Jupiter, Saturn is mostly a ball of hydrogen and helium.'
    },
    {
        image: './src/resources/img/slider/neptuneImage.jpg',
        title: 'Neptune',
        info: 'Neptune is dark, cold, and very windy. It\'s the last of the planets in our solar system. It\'s more than 30 times as far from the sun as Earth is. Neptune is very similar to Uranus. It\'s made of a thick fog of water, ammonia, and methane over an Earth-sized solid center. Its atmosphere is made of hydrogen, helium, and methane. The methane gives Neptune the same blue color as Uranus. Neptune has six rings, but they\'re very hard to see.'
    },
    {
        image: './src/resources/img/slider/uranusImage.jpg',
        title: 'Uranus',
        info: 'Uranus is made of water, methane, and ammonia fluids above a small rocky center. Its atmosphere is made of hydrogen and helium like Jupiter and Saturn, but it also has methane. The methane makes Uranus blue.'
    },
];
