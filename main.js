import {drawGreetingPage} from './src/ui/content/greetingPage/greetingPage.js';
import {divWrapper} from './src/ui/common/common.js';
import {drawMainPage} from './src/ui/content/mainPage/mainPage.js';
import {mainPageTitles} from './src/resources/data/data.js';

if (localStorage.getItem('logged') === 'null') {
    divWrapper.append(
        drawGreetingPage(
            'Welcome to the Universe',
            'https://edifian.digital/wp-content/uploads/2020/05/edifian-orb.gif'
        )
    );
} else {
    drawMainPage(mainPageTitles);
}
